import React, {useState} from 'react';
import {
  HashRouter,
  Route,
  Switch
} from 'react-router-dom';
import { css } from '@emotion/core';


const override = css`
    display: block;
    margin: 0 auto;
    border-color: #bd9317;
`;

const loading = () => {


  return (
    <React.Fragment>
          <h1 class="loadingh1">RABANEDA-CUERVO TOWERS</h1>
          <p class="loadingp1"></p>
          <p class="loadingp2">HOTEL & ROOM RESERVATIONS</p>
    </React.Fragment>

  )
}

// 
const Register = React.lazy(()=>import('./views/Pages/Register'));
const Login = React.lazy(()=>import('./views/Pages/Login'));
const Rooms = React.lazy(()=>import('./views/Room/Rooms'));
const Home = React.lazy(()=>import('./views/Layout/Home'))
const Images = React.lazy(()=>import('./views/Images/Images'))
const Booking = React.lazy(()=>import('./views/Booking/Bookings'))


const App = () => {
  return (
    <HashRouter>
      <React.Suspense fallback={loading()}>
        <Switch>
          <Route 
            path="/register"
            exact
            name="Register"
            render={props => <Register {...props} />}
          />
          <Route 
            path="/login"
            exact
            name="Login"
            render={props => <Login {...props}/>}
          />
          <Route 
            path="/rooms"
            exact
            name="Rooms"
            render={props=> <Rooms 
                {...props}
              />}
          />
          <Route 
            path="/"
            exact
            name="Home"
            render={props=><Home />}
          />
          <Route 
            path="/upload"
            exact
            name="Upload Images"
            render={props=><Images {...props}/>}
          />
          <Route 
            path="/book"
            exact
            name="Bookings"
            render={props=><Booking {...props}/>}
          />
        
        </Switch>
      </React.Suspense>
    </HashRouter>
  )
}

export default App;

