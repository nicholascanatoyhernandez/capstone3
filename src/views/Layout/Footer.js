import React from 'react';
import Helmet from 'react-helmet';


const Footer = () => {
	return (
<React.Fragment>
		<div class="footerdiv1">
			<div class="footerdiv11">
				<h1 class="footerdiv11h1">The RABANEDA-CUERVOTOWERS.COM offers best rate GUARANTEE</h1>
				<p class="footerdiv11p1"></p>

				<div class="footerdiv112">
					<div class="footerdiv1121">
						<h4 class="footerdiv1121h4">RABANEDA-CUERVO TOWERS</h4>
						<p class="footerdiv1121p1"> </p>
						<p class="footerdiv1121p2" >HOTEL & ROOM RESERVATIONS</p>
					</div>

					<div class="footerdiv1122">
						<h5 class="footerdiv1122h5">Customer Service</h5>
						<p class="footerdiv1122p">FAQs</p>
						<p class="footerdiv1122p">Modify/Cancel Reservations</p>
						<p class="footerdiv1122p">Retrieve Hotel Bill</p>
						<p class="footerdiv1122p">Customer Service</p>
					</div>

					<div class="footerdiv1123">
						<h5 class="footerdiv1123h5">Explore more</h5>
						<p class="footerdiv1123p">Gift Cards</p>
						<p class="footerdiv1123p">Residences</p>
						<p class="footerdiv1123p">News</p>
						<p class="footerdiv1123p">Investor Relations</p>
					</div>


					<div class="footerdiv1124">
						<h5>GET MOBILE APP</h5>

						<div>
							<p>picture here</p>
						</div>

						<p></p>

						<h5>CONNECT WITH RABANEDA-CUERVO TOWERS</h5>
						<ul>
							<li></li>
							<li></li>
							<li></li>
							<li></li>
							<li></li>
						</ul>
					</div>
</div>

			</div>

			<div class="footerdiv12">
				
					<div class="footerdiv1125"></div>
						<ul class="footerdiv1125ul">
							<li>Privacy Policy</li>
							<li>Terms & Conditions</li>
							<li>Cookie Statement</li>
							<li>Security & Safety</li>
							<li>Supply Chain Statement</li>
							<li>© 2020 RABANEDA-CUERVO Corporation</li>
						</ul>
					
				
			</div>
		</div>
</React.Fragment>
	)
}

export default Footer


