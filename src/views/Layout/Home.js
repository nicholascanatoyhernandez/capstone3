import React, {useEffect, useState} from 'react';
import {
	Navbar,
	Footer
} from '../Layout';

import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {Gallery, GalleryImage} from 'react-gesture-gallery';

const images = [
  "https://images.unsplash.com/photo-1559666126-84f389727b9a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1356&q=80",
  "https://images.unsplash.com/photo-1557389352-e721da78ad9f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80",
  "https://images.unsplash.com/photo-1553969420-fb915228af51?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1049&q=80",
  "https://images.unsplash.com/photo-1550596334-7bb40a71b6bc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80",
  "https://images.unsplash.com/photo-1550640964-4775934de4af?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80"
];

const INITIAL_INDEX = 0;

const Home = () => {
	const [user, setUser] = useState({})
	const [index,setIndex] = React.useState(INITIAL_INDEX)

	useEffect(()=>{
		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user)
			console.log(user);
			setUser(user);
		}else{
			window.location.replace("#/register");
		}
	}, [])

	React.useEffect(()=>{
		const interval = setInterval (()=>{
		if (index === images.length - 1){
		setIndex(INITIAL_INDEX)
	}else{
		setIndex(index + 1)
	}
	},2500)
		return()=>clearInterval(interval)
	},[index])

		

	return (
		<React.Fragment>


			<Navbar />

			<section style={{height:"85vh"}} class="section1">
			</section>
			<section class="section2">
				<h1 class="section2h1">Experience somewhere new!</h1>
				<p class="section2p1"></p>
				<p class="section2p2">Carved on 54 hectares of picturesque oceanfront,<strong> RABANEDA-CUERVO TOWERS </strong>is the Manila Entertainment City's iconic integrated resort. Stay, play, dine, relax — all amid the luxurious comforts of a one-of-a-kind, masterfully created space for five-star pleasure and leisure. </p>
				<ul class="section2ul">

					<li class="li1"><img class="image1" src={require('./firstli.jpg')} style={{height:"30vh", borderRadius:"50%"}} />
					</li>



					<li class="li2"><img class="image2" src={require('./secondli.jpg')} style={{height:"30vh", borderRadius:"50%"}}/></li>
					<li class="li3"><img class="image3" src={require('./thirdli.jpg')} style={{height:"30vh", borderRadius:"50%"}}/></li>
					<li class="li4"><img class="image4" src={require('./fourthli.jpg')} style={{height:"30vh", borderRadius:"50%"}}/></li>
					<li class="li5"><img class="image5" src={require('./fifthli.jpg')} style={{height:"30vh", borderRadius:"50%"}}/></li>

				</ul>
			</section>


			<section class="section3">
				<div class="section3div1">
						<div class="section3div11"><img src={require('./section3div11.jpg')} class="imagesection3div11"/></div>
						<div class="section3div12">

							<h5 class="section3div12title">Why Book in RABANEDA-CUERVO Towers?</h5>

							<p class="separator"></p>

							<p class="section3div12h1fontawesome"><i class="fas fa-balance-scale"></i></p>
							<h1 class="section3div12h1">Integrity</h1>
							<p class="section3div12pline"></p>
							<p class="section3div12p">We promote integrity through our respect for individuals, communication of expectations, consistency and fairness in our actions. This integrity permeates through all we do.</p>
							

							<p class="separator"></p>

							<p  class="section3div12h1fontawesome"><i class="fas fa-graduation-cap"></i></p>
							<h1 class="section3div12h1">Excellence</h1>
							<p class="section3div12pline"></p>
							<p class="section3div12p">We commit our best in everything we do, striving for professional excellence by exceeding expectations and distinguishing ourselves through superior performance.</p>
							
							

							

						</div>
				</div>
			</section>


			<section class="section4">
				<div class="section4div1">
					<div class="section4div11">
							

							<p class="section3div12h1fontawesome"><i class="fas fa-users"></i></p>
							<h1 class="section3div12h1">Teamwork</h1>
							<p class="section3div12pline"></p>
							<p class="section3div12p">We work as a team at all levels, recognizing each team player contributes to the team’s success by their competence, preparation, determination, and commitment.</p>
						

							<p class="section3div12h1fontawesome"><i class="fas fa-trophy"></i></p>
							<h1 class="section3div12h1">Achievement</h1>
							<p class="section3div12pline"></p>
							<p class="section3div12p">We continually strive to find better ways to pursue our company and personal goals. RABANEDA-CUERVO Towers is committed to providing training, support and growth opportunities for its team members to ensure a rewarding and secure future.</p>
							

							<p  class="section3div12h1fontawesome"><i class="fas fa-info-circle"></i></p>
							<h1 class="section3div12h1">Innovation</h1>
							<p class="section3div12pline"></p>
							<p class="section3div12p">We foster a culture where independence of thought and personal strengths are valued, where team members are recognized for their creativity, motivation, tenacity, and passion. We encourage innovative thinking that promotes beneficial change for both the individual and the company alike.</p>
							
					</div>
					<div><img src={require('./section4div12.jpg')} class="imagesection4div12"/></div>

				</div>

			</section>

			<section class="section5">
				<div class="section5div1">
					<div class="section5div11"><img class="imagesection5div11" src={require('./card.jpg')}/></div>
					<div class="section5div12"><p class="section5div12p">Earn up free points, free gifts and 3 free nights at RABANEDA-CUERVO TOWERS with the
RABANEDA-CUERVO TOWERS Credit Card</p></div>
					<div class="section5div13"><button class="buttonsection5div13">Learn More</button></div>
				</div>
			</section>

			<section class="section9">
					<p class="section9fontawesome"><i class="fas fa-door-open"></i></p>
					<h1 class="section9h1">The Hotel</h1>
					<p class="section9pline"></p>

					<p class="section9p1">We set the new gold standard for five-star luxury. Lavishly executed in every detail. Unprecedented in its bold take on elegance. It spoils you with rich leisure options and stylishly glides past your expectations.

With 1006 exquisitely spacious rooms offering views of either the Manila Bay or The Fountain, RABANEDA-CUERVO Towers create a profound sense of deep relaxation, underlined by the integrated resort’s own brand of exemplary service featuring the unique blend of Japanese hospitality (magokoro and omotenashi) and Filipino warmth.</p>

					<div class="section9div1">
						
						<div class="section9div11">
							<Gallery
						      style={{
						        background:"none",
						        height: "60vh",
						        width: "60vw",
						      }}
						      index={index}
						      onRequestChange={i => {
						        setIndex(i);
						      }}
						    >
						      {images.map(image => (
						        <GalleryImage objectFit="contain" key={image} src={image} />
						      ))}
   							</Gallery>
					
						</div>

						<div class="section9div12">
							<h3 class="section9div12h3">Room Amenities</h3>
							<p class="section9div12p1">We offer the room amenities that matter most to travelers, and they need to be top quality.</p>
							<ul class="section9div12ul">
								<li><i class="far fa-check-square"></i>Plush pillows and breathable bed linens</li>
								<li><i class="far fa-check-square"></i>Soft, oversized bath towels</li>
								<li><i class="far fa-check-square"></i>Full-sized, pH-balanced toiletries</li>
								<li><i class="far fa-check-square"></i>Complimentary refreshments</li>
								<li><i class="far fa-check-square"></i>A little something special</li>
							</ul>

						</div>
					</div>
			</section>

			<section class="section6">
				<div class="section6div1">
					<div class="section6div11">
						<p class="section6div11fontawesome"><i class="fas fa-utensils"></i></p>
						<h1 class="section6div11h1">The Food</h1>
						<p class="section6div11pline"></p>
						<p class="section6div11p2">Taste all of the world’s flavors from breakfast to lunch and with the diverse dining options at RABANEDA-CUERVO Towers. With over 30 dining options, RABANEDA-CUERVO Towers provides non-stop thrills to excite the palate. Cooking up unforgettable moments, our roster of Michelin-starred chefs’ offerings range from fine dining to comfortably casual, and everything in between, serving traditional classics alongside innovative culinary creations.</p>
					</div>
					<div class="section6div12"><img class="imagesection6div12" src={require('./dining.jpg')}/></div>
				</div>
			</section>


			<section class="section67">
				<p class="section67p"><i class="fas fa-shield-alt"></i></p>
				<h1 class="section67h1">Security</h1>
				<p class="section67pline"></p>
				<div class="section67div1">
					<div class="section67div11"><img class="imagesection67div1" src={require('./security.JPG')}/></div>
					<div class="section67div12">
						<p class="section67p1">RABENEDA-CUERVO TOWERS has 31 highly trained security personnel and 23 trained K9's for suspicious/foreign materials. The building is equipped with 46 CCTV cameras and 5 metal detector doors. The RFID lock key cards can become a centralized means to access the services of the hotel. The cards can become a loyalty card wherein the incentives and freebies to the guests can be administered through the card. This card can be used at various POS enabled outlets within the hotel or for use of services such as elevators, rooms, gym, game parlours, massage parlours, swimming pools etc.
						</p>
						<ul class="section67div12ul">
							<li><img class="imagesection67div12ul1" src={require('./sec1.jpg')}/></li>
							<li><img class="imagesection67div12ul2" src={require('./sec2.png')}/></li>
							<li><img class="imagesection67div12ul3" src={require('./sec3.jpg')}/></li>
							<li><img class="imagesection67div12ul4" src={require('./sec4.jpg')}/></li>
						</ul>
					</div>

				</div>

			</section>


			<section  class="section7">
				<div class="section7div1">
					<div class="section7div11"><img class="imagesection7div11" src={require('./chauffer.jpg')}/></div>

					<div class="section7div12">
						<p class="section7div12pfontawesome"><i class="fas fa-car"></i></p>
						<h4 class="section7div12h4">Hotel Chauffeur</h4>
						<p class="section7div12pline"></p>
						<p class="section7div12p2">The RABANEDA-CUERVO Tower Chauffeurs fleet encompasses the finest chauffeuring vehicles on the road including Mercedes S-Class, Mercedes V-Class, Mercedes E Class, Ranger Rover and Bentley Mulsane. If your preference is for another vehicle or a larger executive coach, we can arrange these too.</p>

					</div>
				</div>
			</section>


			<section class="section8">
				<h1 class="section8h1">You can also download our App! </h1>
				<div class="section8div1">
					<div class="section8div11"><img class="section8div11image" src={require('./phone.png')}/></div>
					<div class="section8div12">
						<h5 class="section8div12h5">How it works? Simple as 123</h5>
						<ol class="section8div12ol">
							<li>Download Rabeneda Cuervo Towers App on Google Play</li>
							<li>Register and fill up the form</li>
							<li>Login using your account</li>
							<li>Start booking</li>
						</ol>
						<div class="section8div121">
							<img class="section8div121image" src={require('./button1.JPG')}/>
							<img class="section8div121image" src={require('./button2.JPG')}/>
						</div>
					</div>

				</div>
			</section>


			<Footer />
		</React.Fragment>
	)
}

export default Home;