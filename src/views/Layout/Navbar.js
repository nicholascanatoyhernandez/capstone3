import React,{useState, useEffect} from 'react';
import {
	Link
} from 'react-router-dom'
import Helmet from 'react-helmet';
import {
	Button
} from 'reactstrap';

const Navbar = () => {
	const [user, setUser] = useState({})

	useEffect(()=>{
		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user)
			
			setUser(user);
		}else{
			console.log(user);
		}
	}, [])

const handleLogout =()=>{
	sessionStorage.clear();
	window.location.replace('#/login');
}
	return (

<React.Fragment>


				<nav class="navbar navbar-expand-md navbar-light" style={{backgroundColor:"#111", height:"15vh"}}>
		<div class="container-fluid py-3" style={{marginTop:"1%"}}>
			<div style={{float:"left"}}>
							<h4 style={{color:"#aaa", letterSpacing:"1px", marginBottom:"1%"}}>RABANEDA-CUERVO TOWERS</h4>
			<p style={{width:"70%", padding:"1%", margin:"1%", marginLeft:"15%"}}></p>
			<p style={{fontSize:"0.5em", color:"#FFCF40", marginLeft:"22%", marginTop:"1%", letterSpacing:"3px"}} >HOTEL & ROOM RESERVATIONS</p>
			</div>

			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse text-center" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active" style={{borderRight:"1px solid white", padding:"0", margin:"0", height:"0.5%"}} >
						<a class="nav-link"><Link to="/"
			        className="text-light"

			        >Home </Link></a>
					</li>
					<li class="nav-item" style={{borderRight:"1px solid white", padding:"0", margin:"0", height:"0.5%"}}>
						<a class="nav-link"><Link 
			        to="/rooms"
			        className="text-light"
			        >Rooms</Link></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" ><Link 
					to="/book"
			        	className="text-light"
			        >My Reservations</Link></a>
					</li>
					<li class="nav-item text-light">
						<a class="nav-link" style={{color:"white"}}>Hello, {user.firstname}</a>
					</li>
					<li class="nav-item">
						<a class="nav-link">
						<Button 
						onClick={handleLogout}
						 style={{"backgroundColor":"white", "color": "#bd9317","border": "1px solid #bd9317"}}
			        	className="logout"
			        >Logout</Button></a>
					</li>
				</ul>
			</div>
	</div>
		</nav>


<Helmet>
      <script src="https://kit.fontawesome.com/21425ab497.js"></script>
  <style>{`

		.navbar{
	padding: .5rem;

}

.navbar-nav li {
	padding-right: 20px;
}

.nav-link{
	font-size: 1.1em !important;
}


`}

</style>
</Helmet>
</React.Fragment>
	)
}

export default Navbar;