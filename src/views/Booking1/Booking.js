import React, {useEffect, useState} from 'react';
import axios from 'axios';
import RoomList from './RoomList'
import {
	Row,
	Container
} from 'reactstrap'
import {ToastContainer, toast} from 'react-toastify';
import {Loading} from '../../globalcomponents';
import {
	Navbar, Footer
} from "../Layout"

const Booking = () => {

	const [rooms, setRooms] = useState([]);
	const [isLoading, setIsLoading] = useState(true);

	

	useEffect(()=>{
		axios.get('http://localhost:4000/admin/showrooms')
		.then(res=>
			setRooms(res.data));
		    setIsLoading(false);
	}, [])








	return (
		<React.Fragment>
			<ToastContainer />
		{isLoading ?
			<Loading />
		:
		<React.Fragment>
		<Navbar />
		<Container>
		<h1 className="text-center py-5">Rooms</h1>
			<Row>
			{rooms.map(room=>(
				<RoomList 
					key={room._id}
					room={room}
				/>
			))}
			</Row>
			</Container>
			<Footer />
		</React.Fragment>
		}
		</React.Fragment>
	)
}

export default Booking;