import React, { Component } from "react";
import { Modal, ModalHeader, ModalBody, Button } from "reactstrap";
import DayPicker, { DateUtils } from "react-day-picker";
import "react-day-picker/lib/style.css";
import moment from 'moment'
import Helmet from 'react-helmet'
import axios from 'axios'
//import RoomForm from "../Rooms/components/RoomForm";
class BookForm extends Component {
  state = {
    from: undefined,
    to: undefined
  };
  handleDayClick = day => {
    const range = DateUtils.addDayToRange(day, this.state);
    this.setState(range);
    //console.log(range);
  };
  handleResetClick() {
    this.setState({
      from: undefined,
      to: undefined
    });
  }
  
  handleSaveBooking = () => {
  const oneDay = 24 * 60 * 60 * 1000;
  const firstDate = new Date(this.state.from);
  const secondDate = new Date(this.state.to);
  const diffDays = Math.round(Math.abs((firstDate - secondDate) / oneDay));
  const totalPayment = parseInt(diffDays) * parseInt(this.props.room.price)
  console.log(totalPayment)
    axios({
      method: "POST",
      url: "http://localhost:4000/addbooking",
      data: {
    userName: "JOHN DOE",
    userId: "u34dfdkjhsf99df798sdf",
    roomName: this.props.room.roomName,
    roomNumber: this.props.room.roomNumber,
    bookedFrom : moment(this.state.from).format('MM/DD/YY'),
    bookedTo: moment(this.state.to).format('MM/DD/YY'),
    totalPayment: totalPayment
      }
    }).then(res => {
      console.log(res.data);
  });

  };
  render() {
    const { from, to } = this.state;
    const modifiers = { start: from, end: to };
    //console.log(from + " - " + to)
  //console.log(from);
  //console.log(to);
    return (
      <div>
        <Modal isOpen={this.props.showForm} toggle={this.props.handleShowForm}>
          <ModalHeader toggle={this.props.handleShowForm}>
            Booking Room Name: {this.props.room.roomName}
          </ModalHeader>
          <ModalBody>
      <p>Room #: {this.props.room.roomNumber}</p>
            <p>{this.props.room.description}</p>
            <DayPicker
              className="Selectable"
              numberOfMonths={2}
              onDayClick={this.handleDayClick}
              selectedDays={[from, { from, to }]}
              modifiers={modifiers}
              disabledDays={{ daysOfWeek: [0] }}
            />
            <Button
      onClick={this.handleSaveBooking}
      >Save Booking</Button>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}
export default BookForm;