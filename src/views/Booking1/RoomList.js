import React, {useState} from 'react';
import {
	Button, 
	Col, 
	Card, 
	CardBody, 
	CardTitle, 
	CardSubtitle, 
	CardText,
	CardImg
} from 'reactstrap'
import BookForm from './BookForm'
import axios from 'axios'

const RoomList = props => {
	
	const [showForm, setShowForm] = useState(false)
	const [bookedDates, setBookedDates] = useState([])

	const room = props.room

	const handleShowForm = () => {
		setShowForm(!showForm)

		axios.get('http://localhost:4000/showbookingsbyroomid/'+room._id)
		.then(res=>{console.log(res.data)
			setBookedDates(res.data.bookedDates)

		})
	}

	return (
		<React.Fragment>
		<BookForm
			showForm={showForm}
			handleShowForm={handleShowForm}
			room={room}
			bookedDates={bookedDates}
		/>
			<Col lg={6}

		 className="mt-4 py-5">
			<Card>
			<CardImg top width="100%" height="300px" src={'http://localhost:4000/' + room.image} alt="Room Image" />
				<CardBody>
					<CardTitle>Room Name: {room.name}</CardTitle>
					<CardSubtitle>Description: {room.description}</CardSubtitle>
					<CardText>Occupancy: {room.occupancy} person/s</CardText>
					<CardText>Per Night: P{room.price}</CardText>
					<Button color="info" onClick={handleShowForm} className="col-lg-12">Book</Button>
				</CardBody>
			</Card>
		</Col>
		</React.Fragment>
	)
}

export default RoomList;