import React, {useState} from 'react';
import {
	Button
} from 'reactstrap';
import axios from 'axios'

const Images = props => {
	const [imagesToSave, setImages] = useState([]);
	const [infoMessage, setMessage] = useState("");
	const [imageUrls, setImageUrls] = useState([])

	const selectImages = e => {
		let images = [];
		for(let i=0; i<e.target.files.length; i++){
			images[i]= e.target.files.item(i)
		}

		images = images.filter(image=>image.name.match(/\.(jpg|JPG|JPEG|jpeg|png|gif)$/))
		let message= `${images.length} valid image(s) selected`;

		setImages(images);
		setMessage(message);
	}

	const uploadImages = () => {
		let urls = [...imageUrls]
		const uploaders = imagesToSave.map(image=>{
			const data = new FormData();
			data.append("image", image, image.name)

			return axios.post('https://mysterious-sands-69676.herokuapp.com/upload', data).then(res=>{
				urls.push(res.data.imageUrl)
				setImageUrls(urls)
			})
		})
	}

	console.log(imageUrls)
	return (
		<React.Fragment>
			<div className="col-lg-6 offset-lg-3">
				<input 
					className="form-control" 
					type="file" 
					multiple 
					onChange={selectImages}
				/>
				<small>{infoMessage}</small>
				<Button
					block
					color="primary"
					onClick={uploadImages}
				>
					Upload Images
				</Button>
				<div>
					{
						imageUrls.map((url, index)=>(
							<div key={index}>
								<img 
									src={"https://mysterious-sands-69676.herokuapp.com/"+url}
									alt="Not available"
								/>
							</div>
						))
					}
				</div>
			</div>
		</React.Fragment>
	)
}

export default Images;