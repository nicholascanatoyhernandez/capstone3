import React from 'react';
import {
	Modal,
	ModalHeader,
	ModalBody,
	Button
} from 'reactstrap';
import {FormInput} from '../../../globalcomponents'

const RoomForm = props => {
	return (
		<React.Fragment>
		<section class="roomform">
<Modal
			isOpen={props.showForm}
			toggle={props.handleShowForm}
		>
			<ModalHeader
				toggle={props.handleShowForm}
				// style={{"backgroundColor":"salmon", "color": "white"}}
			>
			Add Room 
			</ModalHeader>
			<ModalBody>
					<label>Room Image:</label>
				<input
	                className = "form-control"
	                type = "file"
	                accept="image/*"
	                onChange = {(e)=>props.handleRoomImageChange(e)}
                    />
				<FormInput
					label={"Room Name"}
					type={"text"}
					name={"name"}
					placeholder={"Enter Name"}
					required={props.nameRequired}
					onChange={props.handleRoomNameChange}
				/>

				<label >Room Description:</label>
				<textarea
					className="form-control"
					label={"Room Description"}
					type={"text"}
					name={"description"}
					placeholder={"Enter Description"}
					required={props.descriptionRequired}
					onChange={props.handleRoomDescriptionChange}
				/>
				<FormInput
					label={"Room Occupancy"}
					name={"occupancy"}
					type={"number"}
					placeholder={"Enter Room Occupancy"}
					required={props.occupancyRequired}
					onChange={props.handleRoomOccupancyChange}
				/>
				<FormInput
					label={"Price Per Night"}
					type={"number"}
					name={"price"}
					placeholder={"Price Per Night"}
					required={props.priceRequired}
					onChange={props.handleRoomPriceChange}
				/>
		
				<Button
					color="warning"
					onClick={props.handleSaveRoom}
					disabled={props.name!="" && props.description != "" && props.occupancy != "" && props.occupancy > 0 ? false : true && props.price != "" && props.price >0 ? false : true}
				>Add Room</Button>
			</ModalBody>
		</Modal>
		</section>
		</React.Fragment>
	)
}

export default RoomForm;