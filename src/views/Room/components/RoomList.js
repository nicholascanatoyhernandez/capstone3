import React, {useState, useEffect } from 'react';
import BookForm from './BookForm';
import axios from 'axios';

import {
	Button, 
	Col, 
	Card, 
	CardBody, 
	CardTitle, 
	CardSubtitle, 
	CardText,
	Row,
	CardImg,
	Container
} from 'reactstrap'
import {
FormInput
} from '../../../globalcomponents';

const RoomList = props => {

	const [showBookForm, setShowBookForm] = useState(false);
	const [isAdmin, setIsAdmin] = useState(false);

	const user = props.user;
	const room = props.room;

	useEffect(()=>{
		let user = JSON.parse(sessionStorage.user);
		setIsAdmin(user.isAdmin);
	},[]);


	const handleShowBookForm = () => {
		
			setShowBookForm(!showBookForm)
		
	}

	return (
		<React.Fragment>
		<Col lg={6}

		 className="mt-5">
		
			<Card>

				<CardBody className="text-center">
				<CardImg top width="100%" height="300px" src={'http://localhost:4000/' + room.image} alt="Room Image"
				 />
				 	{isAdmin ?
					<h4
					onClick={()=>props.handleNameEditInput(room._id)}
					className="pt-4"
					>{props.showName && props.editId === room._id
					? 
				<FormInput
					defaultValue={room.name}
					type={"text"}
					onBlur={(e)=>props.handleEditName(e, room._id)}
				/>
				: room.name}</h4> 
				   : <h4 className="pt-4">{room.name}</h4>}
				

				{isAdmin ?
					<h6
					onClick={()=>props.handleDescriptionEditInput(room._id)}

					>{props.showDescription && props.editId === room._id
				? 
				<FormInput
					defaultValue={room.description}
					type={"text"}
					onBlur={(e)=>props.handleEditDescription(e, room._id)}
				/>
				: room.description}</h6>
				   : <h6>{room.description}</h6>}

				   {isAdmin ?
				<h6
					onClick={()=>props.handleOccupancyEditInput(room._id)}

					>Maximum {props.showOccupancy && props.editId === room._id
						?
					 	<FormInput 
						defaultValue={room.occupancy}
						type={"number"}
						onBlur={(e)=>props.handleEditOccupancy(e, room._id)}
						/> 
					: room.occupancy
						} person/s</h6>
						  : <h6>Maximum {room.occupancy} person/s</h6>}
						{isAdmin ?  
					<h6
						onClick={()=>props.handlePriceEditInput(room._id)}
						>P{props.showPrice && props.editId === room._id
						?
					 	<FormInput 
						defaultValue={room.price}
						type={"number"}
						onBlur={(e)=>props.handleEditPrice(e, room._id)}
						/> 
					: room.price
						}.00 Per Night
					</h6>
					  : <h6>{room.price}.00 Per Night</h6>}
						
				
							{isAdmin ?
							<Button
							color="danger"
							onClick={()=>props.handleDeleteRoom(room._id)}
							className="col-lg-12 my-3"
							>
							Remove Room
							</Button>
							:
							<Button color="info" onClick={handleShowBookForm} 
							className="col-lg-12 my-3">Book</Button>
							}
			
						
					<BookForm
					showBookForm={showBookForm}
					handleShowBookForm={handleShowBookForm}
					room={room}
					user={user}
					
					/>
				</CardBody>
			</Card>
		</Col>

	

	
			

		

			
		</React.Fragment>
	)
}

export default RoomList;