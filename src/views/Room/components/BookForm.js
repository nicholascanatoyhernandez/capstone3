import React, {useState, useEffect} from 'react';
import {Elements, StripeProvider} from 'react-stripe-elements';
import CheckoutForm from './CheckoutForm';
// ES6 Modules or TypeScript
import Swal from 'sweetalert2'
import { 
	Modal, 
	ModalHeader, 
	ModalBody, 
	Button,
	ModalFooter
	 } from "reactstrap";
import {
	FormInput,
	Calendar
} from '../../../globalcomponents'


import axios from 'axios'
import moment from 'moment';

const BookForm = props => {

const user = props.user;
	// CommonJS
	const Swal = require('sweetalert2')

	

  const [modal, setModal] = useState(false);
  const [nestedModal, setNestedModal] = useState(false);
  const [closeAll, setCloseAll] = useState(false);
  const [bookings, setBookings] = useState([]);
  const [toPay, setToPay] = useState(0);
  const [toDay, setDays] = useState(0);
  const [toStart, setStart] = useState("");
  const [toEnd, setEnd] = useState("");


	const [startDate, setStartDate] = useState("");
	const [endDate, setEndDate] = useState("");

	const handleStartDateChange = (startDate) => {
		setStartDate(startDate);
	  }
	const handleEndDateChange = (endDate) => {
		setEndDate(endDate);
	  }

	  const toggle = () => setModal(!modal);

	const toggleNested = () => 
	 {
	    setNestedModal(!nestedModal);
	    setCloseAll(false);
	 }

	  const toggleAll = () => 
	  {
	    setNestedModal(!nestedModal);
	    setCloseAll(true);
	  }

	  	const handleSaveBook = ()=>{
	  		let startDates = moment(startDate).format("MMMM Do YYYY")
	  		let endDates = moment(endDate).format("MMMM Do YYYY")
			let bookingCode = moment(new Date).format("x");
			let roomName = props.room.name
			let roomId = props.room.rooomId
			let days = Math.floor((endDate - startDate) / (1000*60*60*24))
			let user = props.user.name
			let userId = props.user.id
			let totalPayment = props.room.price * days
			
			setToPay(totalPayment);
			setDays(days);
			setStart(startDates);
			setEnd(endDates);

			axios.post('https://mysterious-sands-69676.herokuapp.com/addbooking',{
				bookingCode: bookingCode,
				roomName: roomName,
				roomId: roomId,
				startDate: startDates,
				endDate: endDates,
				days: days,
				user: user,
				userId: userId,
				totalPayment: totalPayment


			}).then(res=>{
				let newBookings = [...bookings]
				newBookings.push(res.data)
				setBookings(newBookings)
				 
				Swal.fire({
				  position: 'center',
				  icon: 'success',
				  title: 'Booking Successfully',
				  showConfirmButton: false,
				  timer: 1500
				})
			})
		}
  
return (
	<React.Fragment>
		<Modal
			isOpen={props.showBookForm}
			toggle={props.handleShowBookForm}
		>
			<ModalHeader
				toggle={props.handleShowBookForm}
				// style={{"backgroundColor":"salmon", "color": "white"}}
			>
			Booking Form
			</ModalHeader>
			<ModalBody className="text-center">
			<h5 className="text-center py-3">Book Your Date For: </h5>

				<img top width="400px" height="300px" className="col-lg-12 py-3" src={'https://lit-beyond-92561.herokuapp.com/' + props.room.image} alt="Room Image"/>
				<h5>{props.room.name}</h5>
				<h5>P{props.room.price}.00 Per Night</h5>
				<Calendar
				handleStartDateChange={handleStartDateChange}
				handleEndDateChange={handleEndDateChange}


				/>
				

		  <ModalFooter>
				<Button
					color="white"
					onClick={handleSaveBook}
					style={{"backgroundColor":"#bd9317", "color": "white"}}
				>Book Date</Button>

				 <Button 
				 style={{"backgroundColor":"white", "color": "#bd9317","border": "1px solid #bd9317"}}
				 onClick={toggleNested}>Next</Button>
				 <i class="fas fa-arrow-square-right"></i>

				 </ModalFooter>
				<Modal isOpen={nestedModal} toggle={toggleNested} onClosed={closeAll ? toggle : undefined}>
	            <ModalHeader>Nested Modal title</ModalHeader>
	            <ModalBody>Stuff and things</ModalBody>
	            <ModalFooter>
	              <Button color="primary" 
	              onClick={toggleNested}
	              >Done</Button>{' '}
	              <Modal isOpen={nestedModal} toggle={toggleNested} onClosed={closeAll ? toggle : undefined}>
            <ModalHeader>Stripe Payment</ModalHeader>
		            <ModalBody>
		            	<StripeProvider apiKey="pk_test_Nnm27tCigE0OfFUirLnElCZ000A0yVVqXm">
							<div>
								
								<div className="d-flex justify-content-center">
									<Elements>
										<CheckoutForm 
										toPay={toPay}
										toDay={toDay}
										/>
									</Elements>
								</div>
							</div>
						</StripeProvider>

		            </ModalBody>
            <ModalFooter>
              <Button color="success" 
              style={{"backgroundColor":"white", "color": "#bd9317","border": "1px solid #bd9317"}}
              onClick={toggleNested}>Done</Button>{' '}
      
            </ModalFooter>
          </Modal>
	              <Button color="secondary" onClick={toggleAll}>All Done</Button>
	            </ModalFooter>
	          </Modal>
	        </ModalBody>
	      
	        
	      </Modal>

		</React.Fragment>
	)
}

export default BookForm;




