import React, {useEffect, useState} from 'react';
import axios from 'axios'
import {BookList} from './components'
import {
	Col,
	Row,
	Container
} from 'reactstrap'
import moment from 'moment'
import {Navbar, Footer} from '../Layout'

const Bookings = () => {
	const [bookings, setBookings] = useState([])
	const [user, setUser] = useState({})
	

	useEffect(()=>{

		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user)
			if(user.isAdmin){
				axios.get('https://mysterious-sands-69676.herokuapp.com/showbookings').then(res=>{
				setBookings(res.data)
				})
			}else{
				axios.get('https://mysterious-sands-69676.herokuapp.com/showbookingsbyuser/'+user.id).then(res=>{
					setBookings(res.data)
					console.log(res.data)
				})
			}

			setUser(user);

		}else{
			window.location.replace('#/login')
		}

	}, []);

	const handleDeleteBooking = (bookingId) => {
		axios.delete('https://mysterious-sands-69676.herokuapp.com/deletebooking/'+bookingId).then(res=>{
			let index = bookings.findIndex(booking=>booking._id===bookingId);
			let newBookings = [...bookings];
			newBookings.splice(index,1);
			setBookings(newBookings);
		})
	}

	



	return (
		<React.Fragment>
		<Navbar />
			<div className="room">
			<Container className="py-5">
			<div>
				<h1 
				className="text-center py-5">My Bookings</h1>
				
			</div>
		
				<Row>
					
					{bookings.map(booking=>(
						<BookList 
							key={booking._id}
							booking={booking}
							handleDeleteBooking={handleDeleteBooking}
							
						/>
					))}
			
				
				</Row>
				</Container>
				</div>
			<Footer/>
		</React.Fragment>
	)

}
export default Bookings;