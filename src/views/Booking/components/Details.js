import React from 'react';
import {
	Modal,
	ModalHeader,
	ModalBody,
	Button,
	ListGroup,
	ListGroupItem
} from 'reactstrap'

const Details = props => {
	return (
		<Modal
			isOpen={props.showDetails}
			toggle={props.handleShowDetails}
		>
			<ModalHeader
				toggle={props.handleShowDetails}
			>
			Details
			</ModalHeader>
			<ModalBody>
				<h1
					className="text-center"
				>
					Logs for Request #
				</h1>
				<h5>Asset Requested: </h5>
				<h5>Requested By: </h5>

				<h3 className="text-center py-3">Logs:</h3>
			

			</ModalBody>
		</Modal>
	)


}

export default Details;
