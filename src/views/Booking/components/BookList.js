import React, {useState} from 'react';
import axios from 'axios';
import {Details} from '../components';
import {
	Button, 
	Col, 
	Card, 
	CardBody, 
	CardTitle, 
	CardSubtitle, 
	CardText,
	Row,
	Container
} from 'reactstrap'



const BookList = props => {

	const [showDetails, setShowDetails] = useState(false);

	const booking = props.booking;


	const handleShowDetails = () => {
		setShowDetails(!showDetails);
	}

	return (
		<React.Fragment>
		<Col lg={6}

		 className="mt-5">
		
			<Card>
	
				<CardBody className="text-center">
			
				<CardTitle>{booking.roomName}</CardTitle>
					<CardTitle>{booking.startDate} - {booking.endDate}</CardTitle>
					<CardSubtitle></CardSubtitle>

				<CardText></CardText>
					
					
					<Button
					color="danger"
					className="col-lg-12"
					onClick={()=>props.handleDeleteBooking(booking._id)}

					>
					Delete Booking
					</Button>

					<Button 
					color="info" 
					onClick={handleShowDetails} 
					className="col-lg-12 my-2"
					>View Details
					</Button>

					<Details 
					handleShowDetails={handleShowDetails}
					showDetails={showDetails}
					booking={booking}
					/>
					<CardText>Booked {booking.createdAt}</CardText>
				</CardBody>
			</Card>
		</Col>


			
		</React.Fragment>
	)
}

export default BookList;